#include "qmlfunctorslot.h"

#include <QApplication>
#include <QQmlApplicationEngine>

#define BOOST_THREAD_PROVIDES_FUTURE
#include <boost/thread.hpp>
#include <boost/thread/future.hpp>

int main(int argc, char *argv[])
{

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/SimpleQml.qml")));

    QObject *rootObject = engine.rootObjects().first();

    QMLFunctorSlot sloter(&engine, [rootObject](){

        boost::future<QString> future = boost::async([](){
            boost::this_thread::sleep_for(boost::chrono::seconds(2));
            return QString("Hello from  boost future ");
        });

        QMetaObject::invokeMethod(rootObject, "updateMessage",
        Q_ARG(QVariant, "Waiting for future"));

        while (future.get_state() == boost::future_state::waiting)
        {
            QGuiApplication::processEvents();
        }

        QMetaObject::invokeMethod(rootObject, "updateMessage",
        Q_ARG(QVariant, future.get()));
    });

    QObject::connect(rootObject, SIGNAL(qmlSignal(QString)),
                     &sloter, SLOT(CallFunctor()));

    return app.exec();

}
