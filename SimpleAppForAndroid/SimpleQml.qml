import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1

Window {

    id     : main
    visible: true

    function updateMessage(msg) {
        buttonText.text = msg
    }
    signal qmlSignal(string msg)

    Grid {
        columns : 1

        anchors.horizontalCenter : parent.horizontalCenter
        anchors.verticalCenter   : parent.verticalCenter

        Rectangle {

            Text {
                id: buttonText
                text: qsTr("Click me")
                anchors.horizontalCenter : parent.horizontalCenter
                anchors.verticalCenter   : parent.verticalCenter
            }



            objectName: "recObjectName"
            id        : rectangle
            width     : 350
            height    : 90
            color     : "skyblue"
            anchors.horizontalCenter : parent.horizontalCenter
            anchors.verticalCenter   : parent.verticalCenter

            states: State {

                name: "rot";
                when: mouseArea.pressed === true
                PropertyChanges { target: rectangle; rotation: 180; color: "red" }
            }

            transitions: Transition {

                from: ""; to: "rot"; reversible: true
                ParallelAnimation {
                    NumberAnimation { properties: "rotation"; duration: 500; easing.type: Easing.InOutQuad }
                    ColorAnimation  { duration: 500 }
                }
            }

            MouseArea {
                id: mouseArea
                hoverEnabled: true
                anchors.fill: parent

                onReleased: {
                    main.qmlSignal("Hello from QML")
                }

                onEntered:  {
                    rectangle.color = "lightblue"
                }

                onExited:   {
                    rectangle.color = "skyblue"
                }

            }


        }
    }


}
