TEMPLATE = app
CONFIG+= C++11

QT += qml quick widgets

SOURCES += main.cpp \
    qmlfunctorslot.cpp

RESOURCES += \
    Resources.qrc



# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

FORMS +=

HEADERS += \
    qmlfunctorslot.h

DISTFILES += \
    SimpleQml.qml

INCLUDEPATH += C:/boostandroid/include/boost-1_57

android{

    equals(ANDROID_TARGET_ARCH, armeabi-v7a){
        LIBS += -LC:/boostandroid/lib \
        -lboost_thread_pthread-gcc-mt-1_57 \
        -lboost_system-gcc-mt-1_57
    }

    equals(ANDROID_TARGET_ARCH, x86){
        # Add x86 LIBS here
    }
}
